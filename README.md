## Installation

1. Rename project name in ***/package.json***
2. Edit Browser refresh details in ***/webpack.config.js***
3. Rename Theme Name in ***/style.css***
4. cd to theme folder and run
   ```
   npm install
   ```
3. then you can run
  ```
  webpack --watch
  ```
