<?php get_header(); ?>
    <header>
        <h1><?php the_title(); ?></h1>
    </header>
    <main role="main">
        <?php while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
        <?php endwhile; ?>
    </main>
<?php get_footer(); ?>
