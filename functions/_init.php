<?php
remove_action('wp_head', 'rsd_link');

remove_action('wp_head', 'wlwmanifest_link');

remove_action('wp_head', 'index_rel_link');

remove_action('wp_head', 'print_emoji_detection_script', 7 );

remove_action('wp_print_styles', 'print_emoji_styles' );

show_admin_bar( false );

register_nav_menus();

register_sidebars();

add_theme_support( 'html5' );

add_theme_support( 'post-thumbnails' );

add_theme_support( 'custom-background' );

add_theme_support( 'custom-header' );

add_theme_support( 'custom-logo' );

// add_theme_support( 'post-formats', array(
// 	'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio'
// ));

// Move Yoast to bottom of edit page
function yoasttobottom() { return 'low'; }
if(class_exists('WPSEO_Options')) {
	add_filter( 'wpseo_metabox_prio', 'yoasttobottom');
}

// Add ACF options page for global fields
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Globals',
		'menu_title' 	=> 'Globals',
		'menu_slug' 	=> 'theme-global-content',
        'capability' 	=> 'edit_posts',
        'icon_url'      =>'dashicons-admin-site',
        'redirect' 	=> false,
        'position'  => 4
	));
}

// Remove H1 from WYSIWYG editor so SEO doesnt get messed up by client
add_filter( 'tiny_mce_before_init', 'remove_h1_from_editor' );
function remove_h1_from_editor( $settings ) {
    $settings['block_formats'] = 'Paragraph=p;Heading 2=h2;Heading 3=h3;Heading 4=h4;Heading 5=h5;Heading 6=h6;Preformatted=pre;Button=a';
    return $settings;
}