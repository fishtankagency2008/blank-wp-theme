<nav id="nav" role="navigation" aria-label="Main">
	<a href="<?= home_url(); ?>" id="logo">
		<?php if( $logo_id = get_theme_mod( 'custom_logo' ) ) : ?>
			<?= wp_get_attachment_image($logo_id, 'full'); ?>
		<?php else : ?>
			<?= get_bloginfo('name') ?>
		<?php endif; ?>
	</a>
	<?php
		wp_nav_menu( [
			'menu'				=> 'Main',
			'container'			=> '',
		]);
	?>
</nav>
